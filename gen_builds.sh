#!/bin/bash

for VERSION in $(<versions.txt); do
for DIST in $(<dist.txt); do


cat << EOF
build-$(tr '\.:' '_' <<< "$VERSION-$DIST"):
  stage: build
  image: $DIST
  variables:
    OS_NAME: $(tr ':' '_' <<< "$DIST")
    VERSION: $VERSION
  script:
    - ./build.sh
  artifacts:
    paths:
      - "*.tar.xz"
EOF


done
done
