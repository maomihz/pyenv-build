#!/bin/bash

# VERSION=
USER="${USER:-cat}"

VERSIONS_ROOT="/home/$USER/.pyenv/versions"

set -xe

rm -rf pyenv && git clone --depth 1 https://github.com/pyenv/pyenv pyenv

pyenv/plugins/python-build/bin/python-build "$VERSION" "$VERSIONS_ROOT/$VERSION"

tar --owner=0 --group=0 -cvJf "python-$VERSION-$OS_NAME-$USER.tar.xz" "$VERSIONS_ROOT/$VERSION"
